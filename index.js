let gAuth = require("google-oauth-jwt");
var express = require('express');
var config = require('./config.json')
var credentials = require('./credentials.json')
var app = express();
const path = require('path');

const getToken = async() => {

    return new Promise((resolve) => {
        gAuth.authenticate({
                email: credentials.client_email,
                key: credentials.private_key,
                scopes: ['https://www.googleapis.com/auth/dialogflow']
            },
            (err, token) => {
                resolve(token);
            }
        );
    })


}

app.get('/', async(req, res) => {
    res.sendFile(path.join(__dirname + '/static/index.html'))
})

app.get('/token', async(req, res) => {
    // console.log(__dirname);
    let token = await getToken();
    res.status(200).json({ "token": token });
})

var server = app.listen(3000, console.log("server started..."))